package com.progressoft.calculator;

public class DivisionOperator implements Operator{

    public  double apply(double firstOperand, double secondOperand) {
        if (firstOperand == 0)
            throw new ArithmeticException("cannot divide by zero");
        return secondOperand / firstOperand;
    }
}
