package com.progressoft.calculator;

public class SubtractionOperator implements Operator {

    @Override
    public double apply(double firstOperand, double secondOperand) {
        return secondOperand - firstOperand;
    }
}

