package com.progressoft.calculator;

public class MultiplicationOperator implements Operator{
    public  double apply(double firstOperand, double secondOperand) {
        return firstOperand * secondOperand;
    }
}
