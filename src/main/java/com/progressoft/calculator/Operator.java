package com.progressoft.calculator;

public interface Operator {
    double apply(double firstOperand, double secondOperand);
}
