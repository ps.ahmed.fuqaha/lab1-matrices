package com.progressoft.calculator;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

public class Calculator {

    private static HashMap<Character, Operator> operators = new HashMap<>();
    private static HashMap<Character, Double> variables = new HashMap();
    private static HashMap<Character, Integer> precedence = new HashMap<>();


    public Calculator() {
        operators.put('+', new AdditionOperator());
        operators.put('/', new DivisionOperator());
        operators.put('*', new MultiplicationOperator());
        operators.put('-', new SubtractionOperator());

        precedence.put('-',1);
        precedence.put('+',1);
        precedence.put('/',2);
        precedence.put('*',2);
    }

    public double calculate(String equation) {
        double result = resolve(equation);
        return result;
    }

    private double resolve(String input) {
        char[] equation = input.toCharArray();
        Stack<Double> operands = new Stack<>();
        Stack<Character> operations = new Stack<>();

        for (int i = 0; i < equation.length; i++) {
            i = processChar(equation, operands, operations, i);
        }
        resolveRemaining(operands, operations);
        return operands.pop();
    }

    private int processChar(char[] equation, Stack<Double> operands, Stack<Character> operations, int i) {
        if (isNumber(equation[i])) {
            i = readOperand(equation, operands, i);
            i--;
        }
        if (isOperator(equation[i])) {
            pushOperator(operands, operations, equation[i]);
        }
        if (isVariable(equation[i])) {
            pushVariableAsOperand(equation, operands, i);
        }
        if (isBracket(equation[i])) {
            i = evaluateBrackets(equation, operands, i);
        }
        return i;
    }

    private void resolveRemaining(Stack<Double> operands, Stack<Character> operations) {
        while (!operations.empty()) {
            operands.push(performOperation(operands.pop(), operands.pop(), operations.pop()));
        }
    }

    private static void pushVariableAsOperand(char[] equation, Stack<Double> operands, int i) {
        if (alreadyTaken(equation[i]))
            operands.push(variables.get(equation[i]));
        else {
            System.out.println("enter the value of " + equation[i]);
            Scanner scanner = new Scanner(System.in);
            double value = scanner.nextDouble();
            variables.put(equation[i], value);
            operands.push(value);
        }
    }

    private void pushOperator(Stack<Double> operands, Stack<Character> operations, char equation) {
        while (!operations.empty() && hasPrecedence(equation, operations.peek())) {
            operands.push(performOperation(operands.pop(), operands.pop(), operations.pop()));
        }
        operations.push(equation);
    }

    private int evaluateBrackets(char[] equation, Stack<Double> operands, int i) {
        StringBuilder stringBuilder = new StringBuilder();
        i++;
        while (!endOfBracket(equation[i])) {
            stringBuilder.append(equation[i++]);
        }
        operands.push(calculate(stringBuilder.toString()));
        return i;
    }

    private static boolean endOfBracket(char equation) {
        return equation == ')';
    }

    private static boolean isBracket(char c) {
        return c == '(';
    }

    private static int readOperand(char[] equation, Stack<Double> operands, int i) {
        StringBuilder stringBuilder = new StringBuilder();
        while (i < equation.length && (isNumber(equation[i])))
            stringBuilder.append(equation[i++]);
        operands.push(Double.parseDouble(stringBuilder.toString()));
        return i;
    }

    private static boolean alreadyTaken(char equation) {
        return variables.containsKey(equation);
    }

    private static boolean isVariable(char equationChar) {
        return equationChar >= 'a' && equationChar <= 'z';
    }

    private static boolean isOperator(char operator) {
        return operator == '+' || operator == '-' || (operator == '*' || operator == '/');
    }

    private static boolean isNumber(char equationChar) {
        return (equationChar >= '0' && equationChar <= '9') || equationChar == '.';
    }

    private double performOperation(double firstOperand, double secondOperand, char operation) {
        Character operationKey = operation;
        Operator operator = operators.get(operationKey);
        if (operator == null)
            throw new IllegalArgumentException("Invalid operator: "+ operation);
        return operator.apply(firstOperand,secondOperand);
    }

    private static boolean hasPrecedence(char firstOperation, char secondOperation) {
        return precedence.get(firstOperation) >= precedence.get(secondOperation);
    }
}