package com.progressoft.calculator;

public class AdditionOperator implements Operator {
    @Override
    public double apply(double firstOperand, double secondOperand) {
            return firstOperand + secondOperand;
    }
}
