import com.progressoft.calculator.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    @Test
    public void givenValidEquation_whenCalculate_thenReturnResult() {
        Calculator calculator = new Calculator();
        double result = calculator.calculate("2+3");
        double expected = 5;
        Assertions.assertEquals(result,expected);
    }

    @Test
    public void givenEquationWithDivisionByZero_whenCalculate_thenThrowException() {
        Calculator calculator = new Calculator();
        String equation = "2/0";
        ArithmeticException exception = Assertions.assertThrows(ArithmeticException.class, () -> calculator.calculate(equation));
        Assertions.assertEquals("cannot divide by zero",exception.getMessage());
    }

    @Test
    public void givenValidEquationWithBrackets_whenCalculate_thenReturnResult() {
        Calculator calculator = new Calculator();
        double result = calculator.calculate("2*(5+4)");
        double expected = 18;
        Assertions.assertEquals(result,expected);
    }
}
